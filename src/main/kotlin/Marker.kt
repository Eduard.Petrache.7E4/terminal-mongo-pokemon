import org.bson.types.ObjectId

data class Marker(
    val _id: ObjectId = ObjectId(),
    val name: String = "",
    val latitude: String = "",
    val longitude: String = "",
    val owner_id: String = "",
    val picture: ByteArray? = null,
    val type: String = ""
)
