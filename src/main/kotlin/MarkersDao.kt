import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Updates
import org.bson.Document
import org.bson.types.ObjectId

class MarkersDao(database: MongoDatabase, val userID: String) {
    private val collection: MongoCollection<Document> = database.getCollection("markers")

    fun listMarkers(): List<Marker> {
        return collection.find(Filters.eq("owner_id", userID)).map {
            Marker(
                _id = it.getObjectId("_id"),
                name = it.getString("name"),
                latitude = it.getString("latitude"),
                longitude = it.getString("longitude"),
                owner_id = it.getString("owner_id"),
                picture = it.get("picture", ByteArray::class.java),
                type = it.getString("type")
            )
        }.toList()
    }

    fun insertItem(name: String, latitude: String, longitude: String) {
        val newMarker = Document("name", name)
            .append("latitude", latitude)
            .append("longitude", longitude)
            .append("owner_id", userID)
            .append("type", "")

        collection.insertOne(newMarker)
    }

    fun updateTitle(markerId: ObjectId, newTitle: String) {
        collection.updateOne(Filters.eq("_id", markerId), Updates.set("name", newTitle))
    }

    fun deleteItem(markerId: ObjectId) {
        collection.deleteOne(Filters.eq("_id", markerId))
    }

    fun updateMarkerType(markerId: ObjectId, newType: String) {
        collection.updateOne(Filters.eq("_id", markerId), Updates.set("type", newType))
    }
}
