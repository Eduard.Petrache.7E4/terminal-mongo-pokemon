object ServiceLocator {
    val mongoManager = MongoManager()

    fun getMarkersDao(username: String): MarkersDao {
        return MarkersDao(mongoManager.database, username)
    }
}
