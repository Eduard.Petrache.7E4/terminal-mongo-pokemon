import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoDatabase
import org.bson.Document

class MongoManager {
    private val connectionString = ConnectionString("mongodb://Eduard1234:Eduard1234@eu-west-2.aws.realm.mongodb.com:27020/?authMechanism=PLAIN&authSource=%24external&ssl=true&appName=application-1-nlfoz:mongodb-atlas:local-userpass")
    private val mongoClient = MongoClients.create(
        MongoClientSettings.builder()
            .applyConnectionString(connectionString)
            .build()
    )
    val database: MongoDatabase = mongoClient.getDatabase("PokemonDB")

    fun register(username: String, password: String) {
        val usersCollection = database.getCollection("users")
        val newUser = Document("username", username).append("password", password)
        usersCollection.insertOne(newUser)
    }

    fun login(username: String, password: String): Boolean {
        val usersCollection = database.getCollection("users")
        val user = usersCollection.find(Document("username", username).append("password", password)).first()
        return user != null
    }
}
